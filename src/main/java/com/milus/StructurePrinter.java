package com.milus;

import org.apache.commons.lang3.StringUtils;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.util.List;

public class StructurePrinter {
    private final ZooKeeper zooKeeper;
    private final Watcher watcher;

    public StructurePrinter(ZooKeeper zooKeeper, Watcher watcher) {
        this.zooKeeper = zooKeeper;
        this.watcher = watcher;
    }

    public void printStructure(String znode) throws KeeperException, InterruptedException {
        printStructure(znode, 0);
    }

    private void printStructure(String path, int indent) throws KeeperException, InterruptedException {
        try {
            List<String> children = zooKeeper.getChildren(path, watcher);
            System.out.print(
                    StringUtils.repeat("  ", indent)
            );
            System.out.println(path);    for (String child : children) {
                printStructure(path + "/" + child, indent + 1);
            }
        } catch (KeeperException | InterruptedException e) {
        }
    }
}
