package com.milus;


import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Client {
    private final Scanner scanner = new Scanner(System.in);

    private final String zookeperAddr;
    private final String znode;
    private final String[] exec;
    private ZNodeWatcher zNodeWatcher;

    public Client(@NonNull String zookeperAddr, @NonNull String znode, @NonNull String[] exec) {
        this.zookeperAddr = zookeperAddr;
        this.znode = znode;
        this.exec = exec;
    }


    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length < 3) {
            System.err
                    .println("USAGE: Client <host> <followed-znode> program [args ...]");
            System.exit(1);
        }
        new Client(args[0], args[1], extractProgramTuRun(args)).run();
    }

    private void run() throws IOException, InterruptedException {
        Executor executor = new Executor(exec);
        zNodeWatcher = new ZNodeWatcher(zookeperAddr, znode);
        zNodeWatcher.observeCreatedZNodeEvent(event -> executor.runProgram());
        zNodeWatcher.observeDeletedZNodeEvent(event -> executor.stopProgram());
        zNodeWatcher.observeHierarchyChangedEvent(event -> zNodeWatcher.printChildrenRecursive());
        keepAlive();

    }

    private void keepAlive() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(() -> {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Do you want to do sth special?");
            System.out.println("Press c - if you want to show hierarchy of the children");
            while (scanner.hasNext()) {
                String command = scanner.next();
                if (StringUtils.startsWith(command, "c")) {
                    zNodeWatcher.printChildrenRecursive();
                }
            }
        });
        executorService.shutdown();
    }

    private static String[] extractProgramTuRun(@NonNull String[] args) {
        return Arrays.copyOfRange(args, 2, args.length);
    }
}
