package com.milus;

import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;

public class Executor {
    private static final Logger LOGGER = LoggerFactory.getLogger(Executor.class);
    private final String[] exec;

    private Optional<Process> process = Optional.empty();

    public Executor(@NonNull String[] exec) {
        this.exec = exec;
    }

    public void runProgram() {
        try {
            stopProgram();
            LOGGER.info("Starting process");
            process = Optional.of(Runtime.getRuntime().exec(exec));
            new BackgroundStreamWriter(process.get().getInputStream(), System.out);
            new BackgroundStreamWriter(process.get().getErrorStream(), System.err);
        } catch (IOException e) {
            LOGGER.warn("Exception from process", e);
        }
    }

    public void stopProgram() {
        if (!process.isPresent()) {
            return;
        }
        try {
            LOGGER.info("Stopping process");
            process.get().destroy();
            process.get().waitFor();
        } catch (InterruptedException e) {
            LOGGER.warn("Interrupted stopping", e);
        }
    }

    private static class BackgroundStreamWriter extends Thread {
        private final OutputStream os;
        private final InputStream is;

        BackgroundStreamWriter(@NonNull InputStream is, @NonNull OutputStream os) {
            this.is = is;
            this.os = os;
            start();
        }

        public void run() {
            byte b[] = new byte[80];
            int rc;
            try {
                while ((rc = is.read(b)) > 0) {
                    os.write(b, 0, rc);
                }
            } catch (IOException e) {
            }

        }
    }
}
