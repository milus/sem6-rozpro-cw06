package com.milus;

import com.milus.event.CreatedZNodeEvent;
import com.milus.event.DeletedZNodeEvent;
import com.milus.event.HierarchyChangedEvent;
import lombok.NonNull;
import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class ZNodeWatcher implements Watcher, AsyncCallback.StatCallback {
    private static final Logger LOGGER = LoggerFactory.getLogger(ZNodeWatcher.class);

    private byte prevData[];

    private final ZooKeeper zooKeeper;
    private final String znode;
    private Optional<Consumer<CreatedZNodeEvent>> createdZNodeEventConsumer = Optional.empty();
    private Optional<Consumer<DeletedZNodeEvent>> deletedZNodeConsumer = Optional.empty();
    private Optional<Consumer<HierarchyChangedEvent>> hierarchyChangedEventConsumer = Optional.empty();

    public ZNodeWatcher(@NonNull String zookeperAddr, @NonNull String znode) throws IOException {
        this.znode = znode;
        zooKeeper = new ZooKeeper(zookeperAddr, 3000, this);
        zooKeeper.exists(znode, true, this, null);
    }

    public void observeCreatedZNodeEvent(Consumer<CreatedZNodeEvent> listener) {
        createdZNodeEventConsumer = Optional.ofNullable(listener);
    }

    public void observeDeletedZNodeEvent(Consumer<DeletedZNodeEvent> listener) {
        deletedZNodeConsumer = Optional.ofNullable(listener);
    }

    public void observeHierarchyChangedEvent(Consumer<HierarchyChangedEvent> listener) {
        hierarchyChangedEventConsumer = Optional.ofNullable(listener);
    }

    @Override
    public void process(WatchedEvent event) {
        String path = event.getPath();
        if (event.getType() == Event.EventType.NodeChildrenChanged) {
            hierarchyChangedEventConsumer.ifPresent(c -> c.accept(new HierarchyChangedEvent()));
            registerWatchForChildren();
        } else {
            if (path != null && path.equals(znode)) {
                // Something has changed on the node, let's find out
                registerWatch();
            }
        }
    }

    @Override
    public void processResult(int rc, String path, Object ctx, Stat stat) {
        boolean exists;
        switch (rc) {
            case KeeperException.Code.Ok:
                exists = true;
                break;
            case KeeperException.Code.NoNode:
                exists = false;
                break;
            case KeeperException.Code.SessionExpired:
            case KeeperException.Code.NoAuth:
                return;
            default:
                // Retry errors
                registerWatch();
                return;
        }

        byte b[] = null;
        if (exists) {
            try {
                b = zooKeeper.getData(znode, false, null);
            } catch (KeeperException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                return;
            }
        }
        if (b == null && b != prevData) {
            //node has been deleted
            deletedZNodeConsumer.ifPresent(c -> c.accept(new DeletedZNodeEvent()));
            prevData = null;
        } else if (b != null && b != prevData) {
            registerWatchForChildren();
            createdZNodeEventConsumer.ifPresent(c -> c.accept(new CreatedZNodeEvent()));
            prevData = b;
        }
    }

    private List<String> registerWatchForChildren() {
        try {
            return zooKeeper.getChildren(znode, this);
        } catch (KeeperException | InterruptedException e) {
            LOGGER.warn("Couldn't register watch for children", e);
            return Collections.emptyList();
        }
    }

    private void registerWatch() {
        zooKeeper.exists(znode, true, this, null);
    }

    public void printChildrenRecursive() {
        try {
            new StructurePrinter(zooKeeper, this).printStructure(znode);
        } catch (KeeperException | InterruptedException e) {
            LOGGER.warn("Couldn't register watch for children", e);
        }
    }
}
